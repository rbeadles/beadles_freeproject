﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public GameObject player;
	public float distance =10f;
	public float height = 5f;
	public float heightdampening = 2.0f;
	public float rotationdampening =3.0f;

	void LateUpdate(){
		//Stops cameras from being set before tank spawns
		if (!player)
			return;
		
		float wantedRotationAngle = player.transform.eulerAngles.y;
		float wantedHeight = player.transform.position.y + height;

		float currentRotationAngle = transform.eulerAngles.y;
		float currentHeight = transform.position.y;

		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationdampening * Time.deltaTime);
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightdampening * Time.deltaTime);

		var currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);

		transform.position = player.transform.position;
		transform.position -= currentRotation * Vector3.forward * distance;

		transform.position = new Vector3 (transform.position.x, currentHeight, transform.position.z);
		transform.LookAt (player.transform);

	}
}



/*
 * BACKUP CODE
 * 
 * public GameObject player;

	Vector3 offset;

	bool cameraSet = false;

	void Update(){
		if (GameObject.FindGameObjectsWithTag("Player").Length == 2 && cameraSet == false){	
			offset = transform.position - player.transform.position;
			cameraSet = true;
		}
	}

	void LateUpdate(){
		if (cameraSet) {
			transform.position = player.transform.position + offset;
		}
	}*/
﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int m_NumRoundsToWin = 5;        
    public float m_StartDelay = 3f;         
    public float m_EndDelay = 3f;           
    public CameraControl m_CameraControl;   
    public Text m_MessageText;              
    public GameObject m_TankPrefab;         
    public TankManager[] m_Tanks;  
	public int tankNum;

    private int m_RoundNumber;              
    private WaitForSeconds m_StartWait;     
    private WaitForSeconds m_EndWait;       
    private TankManager m_RoundWinner;
    private TankManager m_GameWinner;       

	public CameraFollow[] camfollow;

	public Camera camLeft;
	public Camera camRight;

	public GameObject[] lava;
	float totalTime;

	public void BeginGame()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnAllTanks();
        SetCameraTargets();

        StartCoroutine(GameLoop());
    }


    private void SpawnAllTanks()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].m_Instance =
                Instantiate(m_TankPrefab, m_Tanks[i].m_SpawnPoint.position, m_Tanks[i].m_SpawnPoint.rotation) as GameObject;
			camfollow [i].player = m_Tanks [i].m_Instance;
            m_Tanks[i].m_PlayerNumber = i + 1;
            m_Tanks[i].Setup();
			tankNum += 1;
        }
    }


    private void SetCameraTargets()
    {
        Transform[] targets = new Transform[m_Tanks.Length];
        for (int i = 0; i < targets.Length; i++)
        {
            targets[i] = m_Tanks[i].m_Instance.transform;
        }

        m_CameraControl.m_Targets = targets;

    }


    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

        if (m_GameWinner != null)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            StartCoroutine(GameLoop());
        }
    }


    private IEnumerator RoundStarting()
    {
		ResetAllTanks ();
		DisableTankControl ();

		m_CameraControl.SetStartPositionAndSize ();

		m_RoundNumber++;
		m_MessageText.text = "ROUND " + m_RoundNumber;
		yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
		EnableTankControl ();
		m_MessageText.text = string.Empty;

		while (!OneTankLeft ()) {
			EndGame ();
			yield return null;
		}
    }


    private IEnumerator RoundEnding()
    {
		DisableTankControl ();

		m_RoundWinner = null;

		m_RoundWinner = GetRoundWinner ();

		if (m_RoundWinner != null) {
			m_RoundWinner.m_Wins++;
		}
		totalTime = 0;
		for (int i = 0; i < lava.Length; i++) {
			if (i == 0) {
				lava [i].transform.position = new Vector3 (100f, 0.9f, 0f);
			} else if (i == 1) {
				lava [i].transform.position = new Vector3 (-100f, 0.9f, 0f);
			} else if (i == 2) {
				lava [i].transform.position = new Vector3 (0f, 0.9f, 100f);
			} else if (i == 3) {
				lava [i].transform.position = new Vector3 (0f, 0.9f, -100f);
			}
		}

		m_GameWinner = GetGameWinner ();

		string message = EndMessage ();
		m_MessageText.text = message;

        yield return m_EndWait;
    }


    private bool OneTankLeft()
    {
        int numTanksLeft = 0;

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                numTanksLeft++;
        }

        return numTanksLeft <= 1;
    }


    private TankManager GetRoundWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
			if (m_Tanks [i].m_Instance.activeSelf) {
				shrinkScreen (i);
				return m_Tanks [i];
			}

        }

        return null;
    }


    private TankManager GetGameWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
			if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                return m_Tanks[i];
        }

        return null;
    }


    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_RoundWinner != null)
            message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

        message += "\n\n\n\n";

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
        }

        if (m_GameWinner != null)
            message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

        return message;
    }


    private void ResetAllTanks()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].Reset();
        }
    }


    private void EnableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].EnableControl();
        }
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].DisableControl();
        }
    }

	private void shrinkScreen(int i){
		float diff = .125f;
		if (i == 0) {
			print ("Blue Wins");

			camLeft.rect =  new Rect(0,0,camLeft.rect.width-diff,1);
			camRight.rect = new Rect(camLeft.rect.width,0,camRight.rect.width+diff,1);

//			if (m_Tanks [1].m_Wins > 0) {
//				m_Tanks [1].m_Wins--;
//			} else {
//				m_Tanks [0].m_Wins++;
//			}

		} else if (i == 1) {
			print ("Red Wins");

			camLeft.rect =  new Rect(0,0,camLeft.rect.width+diff,1);
			camRight.rect = new Rect(camLeft.rect.width,0,camRight.rect.width-diff,1);

//			if (m_Tanks [0].m_Wins > 0) {
//				m_Tanks [0].m_Wins--;
//			} else {
//				m_Tanks [1].m_Wins++;
//			}
//
		} else {
			print ("ERROR");
		}
		//print (m_Tanks [i].m_Wins);
	}

	private void EndGame(){
		totalTime += Time.deltaTime;
		float step = .15f * Time.deltaTime;
		if (totalTime >= 2) {
			for(int i = 0; i<lava.Length; i++){
				lava[i].transform.position = Vector3.MoveTowards(lava[i].transform.position,Vector3.zero, step);

			}

		}


	}
}